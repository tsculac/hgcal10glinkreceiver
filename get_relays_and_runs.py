#!/usr/bin/python3

import sys
import os

home_dir = sys.argv[1]
all_files = os.listdir(home_dir)

paths = []
file = open("ProcessAllData.sh", "w")
file.write("#!/usr/bin/env bash" + "\n")

for f in all_files:
  if "Relay1695587722" not in f: continue
  relay_dir = os.path.join(home_dir, f)
  all_files_r = os.listdir(relay_dir) # a lot of duplicates here

  #Getting rid of the duplicates
  all_cleaned_files_r = []
  for r_clean in all_files_r:
    if "Run" not in r_clean: continue
    all_cleaned_files_r.append(r_clean.split("Run")[1].split("_")[0])

  all_cleaned_files_r = sorted(list(set(all_cleaned_files_r)))

  #print(all_cleaned_files_r)

  for r in all_cleaned_files_r:
      for i in range(2): #number of ECON-T: 0 or 1
          for j in range(15): #number of BXs: 1 main +- 4 around it = 9 total (AUGUST) or 1 main +- 7 arouned = 15 (SEPTEMBER)
            text_line = ("./econt_processor.exe" + " " + relay_dir.split("Relay")[1] + " " + str(r) + " " + str(i) + " " + str(j))
            file.write(text_line+ "\n")
            last_run = r

file.close()
