Some instructions to get started with processing the RAW ECONT data from August 2024 test beam

Clone the repository
```
git clone https://gitlab.cern.ch/mknight/hgcal10glinkreceiver.git
cd hgcal10glinkreceiver
```

Create a link to the test beam data
```
ln -s /eos/cms/store/group/dpg_hgcal/tb_hgcal/2024/BeamTestAug/HgcalBeamtestAug2024/ datJul24
```

The `econt_processor.cpp` script processes the raw data and outputs a tTree with Branches related to TPG data.

Compile the script
```
./compile.sh
```
and run the executable like
```
./econt_processor.exe $RELAY_NUMBER $RUN_NUMBER $ECONT_NUMBER $BX_RANGE $N_EVENTS
```
with an example being
```
./econt_processor.exe 1722603879 1722603879 0 7 1000
```

Whenever we triggered, we collected 7 BX worth of trigger data (+-3 either side of central BX) so BX 3 would be the central BX. Note, just because we intended BX 3 to be the central one, if we were not aligned properly, most of the signal may appear in later or earlier BX.

There are some python plotting scripts to help visualise data.
