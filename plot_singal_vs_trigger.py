import argparse
import uproot
import hexplot
import sys
import matplotlib.pyplot as plt
import numpy as np
import os
import tools
import ROOT

import matplotlib.colors as mcolors
import matplotlib.cm as cm

def parse_arguments():
    parser = argparse.ArgumentParser(description="Process and plot ECON-T energy vs scintillator trigger.")
    parser.add_argument("--inFile", type=str, help="Path to the ROOT file.")
    parser.add_argument("--run",    type=str, help="Run number")
    parser.add_argument("--BX",  type=str, help="Total number of BXs", default="7")
    parser.add_argument('--train', type=str, help='Choose train to plot')
    parser.add_argument("--type",   type=str, choices=['BC', 'STC4', 'STC16'], help="Type of algorithm running on ECON-T.")
    return parser.parse_args()

if __name__ == "__main__":
    ''' python3 plot_signal_vs_trigger.py --inFile /eos/user/t/tsculac/BigStuff/HGCAL/TestBeam_Aug24/Run1722698259_bug_fix_v1.root --run 1722698259 --train 0 --type BC'''
    args = parse_arguments()

    order = {"BC": 0,"STC4": 1,"STC16": 2}
    with uproot.open(args.inFile) as file:
        for tc_idx in range(6):
            if(args.type == "STC16" and tc_idx >2): break
            energy_BX = []
            trigger_BX = []
            for i in range(int(args.BX)):
                tree = file[f"train_{args.train}/BX{i}/tree"]
                energy_BX.extend(tree[f"energy_int_unpacker{order[args.type]}_{args.type}_{tc_idx}"].array(library="np"))
                trigger = tree[f"triggerLoc"].array(library="np")
                time_shift = -48 + i * 32
                shifted_trigger = [t + time_shift for t in trigger]
                trigger_BX.extend(list(reversed(shifted_trigger)))

            counts, bins_x, bins_y = np.histogram2d(trigger_BX, energy_BX, bins = [32*7, 50], range = [[0, 7*32],[0,2048]])
            fig, ax = plt.subplots(1)
            cmap0 = mcolors.LinearSegmentedColormap.from_list('', ['white', 'red'])
            # Histogram does not follow Cartesian convention, therefore transpose H for visualization purposes.
            counts = counts.T
            pc = ax.pcolorfast(bins_x, bins_y, counts, cmap='Reds', vmin=0, vmax=1000)
            fig.colorbar(pc, ax=ax)
            ax.set_title(f"Algo {args.type} - (S)TC {tc_idx}")
            ax.set_xlabel("Scintillator trigger raising time")
            ax.set_ylabel("5E4M unpacker energy")
            plt.savefig(f"Relay{args.run}_train{args.train}_{args.type}_{tc_idx}.png")
            plt.savefig(f"Relay{args.run}_train{args.train}_{args.type}_{tc_idx}.pdf")
            plt.clf()
