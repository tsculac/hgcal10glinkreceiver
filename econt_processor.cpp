#include <iostream>
#include <bitset>
#include "TH1D.h"
#include "TH2D.h"
#include "TProfile.h"
#include "TFileHandler.h"
#include "FileReader.h"
#include "TTree.h"
#include "SlinkEoe.h"

// ./econt_processor.exe relayNumber runNumber econtNumber BX

// print all (64-bit) words in event
void event_dump(const Hgcal10gLinkReceiver::RecordRunning *rEvent){
  const uint64_t *p64(((const uint64_t*)rEvent)+1);
  for(unsigned i(0);i<rEvent->payloadLength();i++){
    std::cout << "Word " << std::setw(3) << i << " ";
    std::cout << std::hex << std::setfill('0');
    std::cout << "0x" << std::setw(16) << p64[i] << std::endl;
    std::cout << std::dec << std::setfill(' ');
  }
  std::cout << std::endl;
}

// print all (32-bit) words from first or second column in the event
void event_dump_32(const Hgcal10gLinkReceiver::RecordRunning *rEvent, bool second){
  const uint64_t *p64(((const uint64_t*)rEvent)+1);
  for(unsigned i(0);i<rEvent->payloadLength();i++){
    std::cout << "Word " << std::setw(3) << i << " ";
    std::cout << std::hex << std::setfill('0');
    const uint32_t word = second ? p64[i] : p64[i] >> 32;
    std::cout << "0x" << std::setw(8) << word << " ";
    std::cout << std::dec << std::setfill(' ');
    unsigned n = second ? p64[i] : p64[i] >> 32;
    std::bitset<32> b(n);
    std::cout << std::setw(32) << b.to_string() << std::endl; // add binary output for easier debuging
  }
  std::cout << std::endl;
}

bool is_cafe_word(const uint64_t word) {
  return (word >> 32) == 0xcafecafe;
}

// returns location in this event of the n'th 0xfecafe... line
int find_cafe_word(const Hgcal10gLinkReceiver::RecordRunning *rEvent, int n_packet, int cafe_word_loc = -1) {
  const uint64_t *p64(((const uint64_t*)rEvent)+1);

  // Print the initial state
  //std::cout << "Looking for the " << n_packet << "-th occurrence of cafe word." << std::endl;

  if (cafe_word_loc > 0) {
    //std::cout << "Checking specific location: " << cafe_word_loc << std::endl;
    if (is_cafe_word(p64[cafe_word_loc])) {
      //std::cout << "Found at location " << cafe_word_loc << std::endl;
      return cafe_word_loc;
    }
  }

  int cafe_counter = 0;
  int cafe_word_idx = -1;
  //std::cout << "Payload length: " << rEvent->payloadLength() << std::endl; //total numer of words in the file

  for(unsigned i(0);i<rEvent->payloadLength();i++){
    const uint64_t word = p64[i];
    if (is_cafe_word(word)) { // if word == 0xcafecafe
      cafe_counter++;
      if (cafe_counter == n_packet){
        cafe_word_idx = i;
        break;
      }
    }
  }

  // if (cafe_word_idx == -1) {
  //   std::cout << "Could not find cafe word" << std::endl;
  //   // exit(0);
  // }
  // else {
  return cafe_word_idx;
  // }
}

uint32_t pick_bits32(uint32_t number, int start_bit, int number_of_bits) {
  // Create a mask to extract the desired bits.
  uint32_t mask = (1 << number_of_bits) - 1;
  // Shift the number to the start bit position.
  number = number >> (32 - start_bit - number_of_bits);
  // Mask the number to extract the desired bits.
  uint32_t picked_bits = number & mask;

  return picked_bits;
}

uint64_t pick_bits64(uint64_t number, int start_bit, int number_of_bits) {
  // Create a mask to extract the desired bits.
  uint64_t mask = (1 << number_of_bits) - 1;
  // Shift the number to the start bit position.
  number = number >> (64 - start_bit - number_of_bits);
  // Mask the number to extract the desired bits.
  uint64_t picked_bits = number & mask;

  return picked_bits;
}

int get_channel_ID(const Hgcal10gLinkReceiver::RecordRunning *rEvent, int n_header, int cafe_word_loc = -1){
      const uint64_t *p64 = ((const uint64_t*)rEvent) + 1;

    int header_loc = find_cafe_word(rEvent, n_header, cafe_word_loc);
    //std::cout << "header_location " << header_loc << std::endl;
    const uint64_t header = p64[header_loc];
    // Extract the ID channel bits
    const uint64_t channel_ID = pick_bits64(header, 48, 8);
    std::cout <<  "Channel ID " << std::dec << channel_ID << std::endl;
    return channel_ID;
}

//get number of words in each BX
int get_n_words_in_BX(const Hgcal10gLinkReceiver::RecordRunning *rEvent, int n_header =1, int cafe_word_loc = -1) {
  const uint64_t *p64 = ((const uint64_t*)rEvent) + 1;

    int header_loc = find_cafe_word(rEvent, n_header, cafe_word_loc);
    //std::cout << "header_location " << header_loc << std::endl;
    const uint64_t header = p64[header_loc];
    // Extract the 6th group of 4 bits
    const uint64_t n_words_in_BX = pick_bits64(header, 40, 4);
    return n_words_in_BX;
  }

// // // // // // // // get info from header // // // // // // // //

// get the total number of BX stored in an event
int get_BXNumber(const Hgcal10gLinkReceiver::RecordRunning *rEvent, int n_header, int cafe_word_loc = -1) {
  const uint64_t *p64(((const uint64_t*)rEvent)+1);

  int header_loc = find_cafe_word(rEvent, n_header, cafe_word_loc);
  const uint64_t header = p64[header_loc];
  const uint64_t n_words_in_packet = pick_bits64(header,56,8);
  const uint64_t n_words_in_BX = pick_bits64(header,40,4);

  int BXNumber = -1;
  if ( n_words_in_BX != 0 ) {
    BXNumber = n_words_in_packet / n_words_in_BX;
  } else {
    std::cout << "Number of words per BX not initialized!" << std::endl;
    std::abort();
  }
  return BXNumber;
}

// get the number of words in BX
int get_word_in_BX(const Hgcal10gLinkReceiver::RecordRunning *rEvent, int n_header = 1, int cafe_word_loc = -1) {
  const uint64_t *p64(((const uint64_t*)rEvent)+1);

  int header_loc = find_cafe_word(rEvent, n_header, cafe_word_loc);
  const uint64_t header = p64[header_loc];
  return pick_bits64(header,40,4);
}

// get packet size
int get_packet_size(const Hgcal10gLinkReceiver::RecordRunning *rEvent, int n_header, int cafe_word_loc = -1) {
  const uint64_t *p64(((const uint64_t*)rEvent)+1);

  int header_loc = find_cafe_word(rEvent, n_header, cafe_word_loc);
  const uint64_t header = p64[header_loc];
  return pick_bits64(header,56,8);
}

// get buffer status
int get_buffer_status(const Hgcal10gLinkReceiver::RecordRunning *rEvent, int n_header, int cafe_word_loc = -1) {
  const uint64_t *p64(((const uint64_t*)rEvent)+1);

  int header_loc = find_cafe_word(rEvent, n_header, cafe_word_loc);
  const uint64_t header = p64[header_loc];
  return pick_bits64(header,44,4);
}

// get channel_id
int get_channel_id(const Hgcal10gLinkReceiver::RecordRunning *rEvent, int n_header, int cafe_word_loc = -1) {
  const uint64_t *p64(((const uint64_t*)rEvent)+1);

  int header_loc = find_cafe_word(rEvent, n_header, cafe_word_loc);
  const uint64_t header = p64[header_loc];
  return pick_bits64(header,48,8);
}

// // // // // // // // // // // // // // // // // // // // // // // //

void set_packet(std::map<int, std::vector<uint64_t>>& packet, const Hgcal10gLinkReceiver::RecordRunning *rEvent,
		int BXNumber, int& BX_id_h, int cafe_word_loc = -1) {

  int word_idx = 0;
  const uint64_t *p64(((const uint64_t*)rEvent)+1);
  // const Hgcal10gLinkReceiver::SlinkEoe *seoe=(const Hgcal10gLinkReceiver::SlinkEoe*)p64;
  // BX_id_h = seoe->bxId();

  for (int n_header=1; n_header<10; n_header++) {
    int start_word_idx = find_cafe_word(rEvent, n_header);
    if (start_word_idx == -1) {
      if (BXNumber == 0) {
        BX_id_h = p64[word_idx+2] & 0x7;
      }
      return;
    }
    int channel_id     = get_channel_id(rEvent, n_header);
    int buffer_status  = get_buffer_status(rEvent, n_header);
    int packet_size    = get_packet_size(rEvent, n_header);
    int n_words_in_BX  = get_word_in_BX(rEvent, n_header);
    int n_BX           = get_BXNumber(rEvent, n_header);

    if (channel_id != n_header-1) {
	std::cout << "Channel from header " << channel_id << ", expected " << n_header << std::endl;
    }

    // std::cout << "loc header " << start_word_idx << " size " << packet_size << " ch_id " << channel_id << " bufstat " \
      << buffer_status << " nofwd_perbx " << n_words_in_BX << " nofBX " << n_BX << std::endl;
    for (unsigned j=0; j<n_words_in_BX; j++) {
      word_idx = start_word_idx + 1 + n_words_in_BX*BXNumber + j;
      packet[channel_id].push_back(p64[word_idx]);
    }
  }
}

// scintillator triggers during BX, find the time within the whole BX window
// possible output is N_BX*[0,32]
std::tuple<int, int> get_scintillator_trigger_loc(const Hgcal10gLinkReceiver::RecordRunning *rEvent, int packet_location) {
  const uint64_t *p64(((const uint64_t*)rEvent)+1);

  int timing_word_start_idx = find_cafe_word(rEvent, packet_location) + 7; // Scintilator data is a 7th word within the packet (right before 0x00000000aaaaaaaa separator)
  // std::cout << "timing_word_start_idx = " << timing_word_start_idx << std::endl;

  int trigger_loc = 0;
  int trigger_width = 0;

  bool trigger = 0; // boolean that tells us if trigger happened

  for(unsigned i(timing_word_start_idx);i<rEvent->payloadLength();i=i+7) // There are total 7 words of scintillator data in each sub-packet
  {
    const uint32_t timing_word = p64[i];

    // std::cout << "timing_word = ";
    // std::cout << std::hex << std::setfill('0');
    // std::cout << "0x" << std::setw(16) << timing_word << std::endl;
    // std::cout << std::dec << std::setfill(' ');

    if (timing_word == 0 && !trigger) trigger_loc = trigger_loc + 32;
    else if (timing_word != 0 && !trigger) {
      // use built in function to find number of leading zeros
      trigger_loc = trigger_loc + __builtin_clz(timing_word);
      trigger = 1;
      trigger_width = 32 - __builtin_clz(timing_word);
   }
    else if (timing_word == 0xffffffff && trigger) trigger_width = trigger_width + 32;
    else if (timing_word != 0 && trigger) trigger_width = trigger_width + (32 - __builtin_ctz(timing_word));

  }


  // if could not find trigger location -> exit
  if (trigger_loc == -1) {
    exit(1);
  }

  return std::make_tuple(trigger_loc, trigger_width);
}

// packet counter is the first 4 bits
uint32_t get_packet_counter(uint32_t* packet) {
  return pick_bits32(packet[0], 0, 4);
}

static uint32_t unpack5E4MToUnsigned(uint32_t flt) {
    assert(flt<0x200);

    uint32_t e((flt>>4)&0x1f);
    uint32_t m((flt   )&0x0f);
    //return (e==0?m:(16+m)<<(e-1));
    if(e==0) return m;
    else if(e==1) return 16+m;
    else return (32+2*m+1)<<(e-2);
}

static uint16_t pack5E4MFromUnsigned(uint32_t erg) {
    if(erg<16) return erg;

    unsigned e(1);
    while(erg>=32) {
      e++;
      erg>>=1;
    }
    return 16*(e-1)+erg;
 }

static uint16_t pack5E4MFrom4E3M(uint8_t flt) {
    assert(flt<0x80);
    uint32_t e((flt>>3)&0xf);
    uint32_t m((flt   )&0x7);
    uint32_t u;
    if(e==0) u=m;
    else if(e==1) u=8+m;
    else u=(16+2*m+1)<<(e-2);

    return pack5E4MFromUnsigned(u);
}

// assigning TC or STCs energies
void set_packet_energies(std::vector<uint16_t>& packet_energies, uint64_t word_energies, int n_TCs, int n_bits_TC) {
  for (int i=0; i<n_TCs; i++) {
    // std::cout << "raw " << (pick_bits64(word_energies, i*n_bits_TC, n_bits_TC) & 0x7F) << std::endl;
    // std::cout << "raw " << pack5E4MFrom4E3M(pick_bits64(word_energies, i*n_bits_TC, n_bits_TC) & 0x7F) << std::endl;
    packet_energies.push_back(pick_bits64(word_energies, i*n_bits_TC, n_bits_TC) & 0x7F);
  }
}

// assigning TC or STCs addresses
void set_packet_locations(std::vector<uint16_t>& packet_locations, uint64_t word_locations, int header, int n_TCs, int n_bits_TC) {
  for (int i=0; i<n_TCs; i++) {
    // std::cout << "raw " << pick_bits64(word_locations, header+i*n_bits_TC, n_bits_TC) << std::endl;
    packet_locations.push_back(pick_bits64(word_locations, header+i*n_bits_TC, n_bits_TC) & 0x3F);
  }
}

void get_unpacked_energies_locations(std::vector<uint16_t>& locations, std::vector<uint32_t>& energies, std::vector<uint64_t> packet, std::string algo) {
  for (int word_idx=1; word_idx<packet.size()-1; ++word_idx) {
    uint16_t econt_word = 0;
    if (algo=="BC") {
      econt_word = packet[word_idx] & 0xFFFF;
      // std::cout << "unpacked " << ((econt_word >> 6)&0x1FF) << std::endl;
      // std::cout << "energy int " << unpack5E4MToUnsigned(((econt_word >> 6)&0x1FF)) << std::endl;
    }
    if (algo=="STC4") {
      econt_word = (packet[word_idx]>>16) & 0xFFFF;
    }
    if (algo=="STC16") {
      if (word_idx > 3) break; // STC16 uses only first 3 words, not all 6
      econt_word = (packet[word_idx]>>(2*16)) & 0xFFFF;
    }
    locations.push_back(econt_word & 0x3F);
    energies.push_back((econt_word >> 6)&0x1FF);
  }
}

void set_packet_config(std::map<int, std::map<int, std::vector<uint64_t>>> packet, std::map<std::string, std::vector<uint16_t>>& packet_locations, \
		       uint16_t& module_sum, std::map<std::string, std::vector<uint16_t>>& packet_energies, \
		       std::map<std::string, std::vector<uint16_t>>& packet_locations_unpacked, \
		       std::map<std::string, std::vector<uint32_t>>& packet_energies_unpacked, \
		       std::vector<std::string> econt_config, int BX_id_h, int BXNumbers, int channel_id, bool aligned, int wantedBX) {
  int word_idx = 0;
  if (aligned) wantedBX = 3; // if no match with Slink header, choose the central BX

  for (std::string algo : econt_config) {
    if (algo == "BC") {
	// std::cout << "=======> BC" << std::endl;
	if (aligned) {
       for (int BXNumber = 0; BXNumber < BXNumbers; ++BXNumber) {
          uint64_t wordBC_locations = pick_bits64(packet[BXNumber][channel_id][word_idx], 0, 48) << 16;
          uint64_t wordBC_energy    = (pick_bits64(packet[BXNumber][channel_id][word_idx], 48, 16) << 48) + ((packet[BXNumber][channel_id][word_idx+1] >> 32) << 16);
	       module_sum = pick_bits64(wordBC_locations, 4, 8);
	       // std::cout << "BXNumber " <<  BXNumber << " header local " << pick_bits64(wordBC_locations, 0, 4) << " header global " << (BX_id_h%8 & 0xF) << std::endl;
	    if ( pick_bits64(wordBC_locations, 0, 4) == ( BX_id_h%8 & 0xF ) ) {
	       set_packet_locations(packet_locations["BC"], wordBC_locations, 4+8, 6, 6);
          set_packet_energies(packet_energies["BC"], wordBC_energy, 6, 7);
	       get_unpacked_energies_locations(packet_locations_unpacked[algo], packet_energies_unpacked[algo], packet[BXNumber][channel_id+2], algo);
	       break;
	    }
	  }
	}
        if (packet_locations["BC"].size()==0 or !aligned) {
          uint64_t wordBC_locations = pick_bits64(packet[wantedBX][channel_id][word_idx], 0, 48) << 16;
          uint64_t wordBC_energy    = (pick_bits64(packet[wantedBX][channel_id][word_idx], 48, 16) << 48) + ((packet[wantedBX][channel_id][word_idx+1] >> 32) << 16);
	       module_sum = pick_bits64(wordBC_locations, 4, 8);
          // std::cout << "BXNumber " <<  wantedBX << " header local " << pick_bits64(wordBC_locations, 0, 4) << " header global " << (BX_id_h%8 & 0xF) << std::endl;
          set_packet_locations(packet_locations["BC"], wordBC_locations, 4+8, 6, 6);
          set_packet_energies(packet_energies["BC"], wordBC_energy, 6, 7);
	       get_unpacked_energies_locations(packet_locations_unpacked[algo], packet_energies_unpacked[algo], packet[wantedBX][channel_id+2], algo);
	}
        word_idx += 1;
    }
    if (algo == "STC4") {
	// std::cout << "=======> STC4A" << std::endl;
	if (aligned) {
	  for (int BXNumber = 0; BXNumber < BXNumbers; ++BXNumber) {
            uint64_t wordSTC4_locations = pick_bits64(packet[BXNumber][channel_id][word_idx], 32, 16) << 48;
            uint64_t wordSTC4_energy    = (pick_bits64(packet[BXNumber][channel_id][word_idx], 48, 16) << 48) + ((packet[BXNumber][channel_id][word_idx+1] >> 32) << 16);
	    // std::cout << BXNumber << " " << pick_bits64(wordSTC4_locations, 0, 4) << " " << BX_id_h%8 << std::endl;
            if ( pick_bits64(wordSTC4_locations, 0, 4) == ( BX_id_h%8 & 0xF ) ) {
              set_packet_locations(packet_locations["STC4"], wordSTC4_locations, 4, 6, 2);
              set_packet_energies(packet_energies["STC4"], wordSTC4_energy, 6, 7);
	      get_unpacked_energies_locations(packet_locations_unpacked[algo], packet_energies_unpacked[algo], packet[BXNumber][channel_id+2], algo);
	      break;
	    }
	  }
	}
        if (packet_locations["STC4"].size()==0 or !aligned) {
          uint64_t wordSTC4_locations = pick_bits64(packet[3][channel_id][word_idx], 32, 16) << 48;
          uint64_t wordSTC4_energy    = (pick_bits64(packet[3][channel_id][word_idx], 48, 16) << 48) + ((packet[3][channel_id][word_idx+1] >> 32) << 16);
          set_packet_locations(packet_locations["STC4"], wordSTC4_locations, 4, 6, 2);
          set_packet_energies(packet_energies["STC4"], wordSTC4_energy, 6, 7);
	  get_unpacked_energies_locations(packet_locations_unpacked[algo], packet_energies_unpacked[algo], packet[3][channel_id+2], algo);
	}
        word_idx += 1;
    }
    if (algo == "STC16") {
	// std::cout << "=======> STC16" << std::endl;
	if (aligned) {
	  for (int BXNumber = 0; BXNumber < BXNumbers; ++BXNumber) {
            uint64_t wordSTC16_locations = pick_bits64(packet[BXNumber][channel_id][word_idx], 32, 16) << 48;
            uint64_t wordSTC16_energy    = (pick_bits64(packet[BXNumber][channel_id][word_idx], 48, 16) << 48) + ((packet[BXNumber][channel_id][word_idx+1] >> 32) << 16);
	    // std::cout << BXNumber << " " << pick_bits64(wordSTC16_locations, 0, 4) << " " << BX_id_h%8 << std::endl;
            if ( pick_bits64(wordSTC16_locations, 0, 4) == ( BX_id_h%8 & 0xF ) ) {
              set_packet_locations(packet_locations["STC16"], wordSTC16_locations, 4, 3, 4);
              set_packet_energies(packet_energies["STC16"], wordSTC16_energy, 3, 9);
	      get_unpacked_energies_locations(packet_locations_unpacked[algo], packet_energies_unpacked[algo], packet[BXNumber][channel_id+2], algo);
	      break;
	    }
	  }
	}
        if (packet_locations["STC16"].size()==0 or !aligned) {
          uint64_t wordSTC16_locations = pick_bits64(packet[3][channel_id][word_idx], 32, 16) << 48;
          uint64_t wordSTC16_energy    = (pick_bits64(packet[3][channel_id][word_idx], 48, 16) << 48) + ((packet[3][channel_id][word_idx+1] >> 32) << 16);
          set_packet_locations(packet_locations["STC16"], wordSTC16_locations, 4, 6, 2);
          set_packet_energies(packet_energies["STC16"], wordSTC16_energy, 6, 7);
	  get_unpacked_energies_locations(packet_locations_unpacked[algo], packet_energies_unpacked[algo], packet[3][channel_id+2], algo);
	}
	word_idx += 1;
    }
  }
}

int main(int argc, char** argv){

  if(argc < 3){
    std::cerr << argv[0] << ": no relay and/or run numbers specified" << std::endl;
    return 1;
  }

  // ECON-Ts configuration: BC, STC4, STC16
  std::vector<std::string> econt_config = {"BC", "STC4", "STC16"};

  //Command line arg assignment, assign relay and run numbers
  unsigned relayNumber(0);
  unsigned runNumber(0);
  unsigned trainNumber(0);
  unsigned BXNumbers(0);
  unsigned n_Events(0);
  unsigned linkNumber(0);
  unsigned aligned(0);
  unsigned wantedBX(0);

  std::istringstream issRelay(argv[1]);
  issRelay >> relayNumber;
  std::istringstream issRun(argv[2]);
  issRun >> runNumber;
  std::istringstream issTrain(argv[3]);
  issTrain >> trainNumber;
  std::istringstream issBX(argv[4]);
  issBX >> BXNumbers;
  std::istringstream issEvent(argv[5]);
  issEvent >> n_Events;
  std::istringstream issAligned(argv[6]);
  issAligned >> aligned;
  std::istringstream issWantedBX(argv[7]);
  issWantedBX >> wantedBX;

  //Create the file reader
  Hgcal10gLinkReceiver::FileReader _fileReader;

  //Make the buffer space for the records
  Hgcal10gLinkReceiver::RecordT<4095> *r(new Hgcal10gLinkReceiver::RecordT<4095>);

  //Set up specific records to interpet the formats
  const Hgcal10gLinkReceiver::RecordStarting *rStart((Hgcal10gLinkReceiver::RecordStarting*)r);
  const Hgcal10gLinkReceiver::RecordStopping *rStop ((Hgcal10gLinkReceiver::RecordStopping*)r);
  const Hgcal10gLinkReceiver::RecordRunning  *rEvent((Hgcal10gLinkReceiver::RecordRunning*) r);
  _fileReader.setDirectory(std::string("datJul24/Relay")+argv[1]);
  _fileReader.openRun(runNumber,linkNumber);

  int nEvents = 0, event = 0;
  int trigger_loc = -1, trigger_width = -1;

  // root tree and branches definition
  TTree *tree = new TTree("tree", "Tree with vector branches");

  TBranch*** branch_raw_energy    = new TBranch**[3];
  TBranch*** branch_5E4M_unpacker = new TBranch**[3];
  TBranch*** branch_int_unpacker  = new TBranch**[3];
  TBranch*** branch_raw_location  = new TBranch**[3];
  TBranch*** branch_unpacker_loc  = new TBranch**[3];
  TBranch**  branch_modSum   = new TBranch*[3];

  tree->Branch("EventNumber",&event,"EventNumber/I");
  tree->Branch("triggerLoc",&trigger_loc,"trigger_loc/I");
  tree->Branch("triggerWidth",&trigger_width,"trigger_width/I");

  uint16_t branchData_raw_energy[3][6] = {0}, branchData_modSum[3] = {0};
  uint16_t branchData_raw_location[3][6] = {0}, branchData_unpacker_loc[3][6] = {0};
  uint32_t branchData_5E4M_unpacker[3][6] = {0};
  uint32_t branchData_int_unpacker[3][6] = {0};

  int iEcont = 0, nTCs = 0;
  std::string name_branch, name_val;
  for (std::string algo : econt_config) {
    branch_raw_energy[iEcont]    = new TBranch*[6];
    branch_5E4M_unpacker[iEcont] = new TBranch*[6];
    branch_int_unpacker[iEcont]  = new TBranch*[6];
    branch_raw_location[iEcont]  = new TBranch*[6];
    branch_unpacker_loc[iEcont]  = new TBranch*[6];
    if (algo == "STC16") { nTCs=3; }
    else { nTCs=6; }
    for (int i=0;i<nTCs;i++) {
      name_branch = "energy_raw_econt" + std::to_string(iEcont) + "_" + algo + "_" + std::to_string(i);
      branch_raw_energy[iEcont][i] = tree->Branch(name_branch.c_str(), &branchData_raw_energy[iEcont][i], (name_branch + "/s").c_str());
      name_branch = "energy_5E4M_unpacker" + std::to_string(iEcont) + "_" + algo + "_" + std::to_string(i);
      branch_5E4M_unpacker[iEcont][i] = tree->Branch(name_branch.c_str(), &branchData_5E4M_unpacker[iEcont][i], (name_branch + "/i").c_str());
      name_branch = "energy_int_unpacker" + std::to_string(iEcont) + "_" + algo + "_" + std::to_string(i);
      branch_int_unpacker[iEcont][i] = tree->Branch(name_branch.c_str(), &branchData_int_unpacker[iEcont][i], (name_branch + "/I").c_str());
      name_branch = "raw_location" + std::to_string(iEcont) + "_" + algo + "_" + std::to_string(i);
      branch_raw_location[iEcont][i] = tree->Branch(name_branch.c_str(), &branchData_raw_location[iEcont][i], (name_branch + "/s").c_str());
      name_branch = "unpacker_loc" + std::to_string(iEcont) + "_" + algo + "_" + std::to_string(i);
      branch_unpacker_loc[iEcont][i] = tree->Branch(name_branch.c_str(), &branchData_unpacker_loc[iEcont][i], (name_branch + "/s").c_str());
    }
    name_branch = "modSum";
    if (algo == "BC") { branch_modSum[iEcont] = tree->Branch(name_branch.c_str(), &branchData_modSum[iEcont], (name_branch + "/s").c_str()); }
    iEcont += 1;
  }

  int econt_cafe_word_loc;

  while(_fileReader.read(r)) {
    if( r->state()==Hgcal10gLinkReceiver::FsmState::Starting ){
      rStart->print();
    }
    else if( r->state()==Hgcal10gLinkReceiver::FsmState::Stopping ){
      rStop->print();
    }
    else{
      nEvents++;
      // std::cout << std::dec << "_____ event _____ " << nEvents << std::endl;

      std::map<int, std::map<int, std::vector<uint64_t>>> packet;
      std::map<std::string, std::vector<uint16_t>> packet_locations;
      std::map<std::string, std::vector<uint16_t>> packet_energies;
      std::map<std::string, std::vector<uint16_t>> packet_locations_unpacked;
      std::map<std::string, std::vector<uint32_t>> packet_energies_unpacked;
      uint16_t module_sum;
      // uint32_t packet_counter;
      int BX_id_h = 0;

      // if (nEvents < 10) {
      // event_dump(rEvent);
      //   continue;
      // }

      std::tie(trigger_loc, trigger_width) = get_scintillator_trigger_loc(rEvent, 6); // Scintillator data is in the 6th packet of payload
      // std::cout << "Trigger location = " << trigger_loc << " Trigger width = " << trigger_width << std::endl;


      for (int BXNumber = 0; BXNumber < BXNumbers; ++BXNumber) {
        set_packet(packet[BXNumber], rEvent, BXNumber, BX_id_h);
      }
      // e-links unpacking
      set_packet_config(packet, packet_locations, module_sum, packet_energies, \
		        packet_locations_unpacked, packet_energies_unpacked, econt_config, \
			BX_id_h, BXNumbers, trainNumber+1, aligned, wantedBX);

      int iEcont = 0, nTCs = 0;
      for (std::string algo : econt_config) {
        if (algo == "STC16") { nTCs=3; }
        else { nTCs=6; }
        for (int i=0;i<nTCs;i++) {
	  branchData_raw_energy[iEcont][i]    = packet_energies[algo][i];
	  branchData_5E4M_unpacker[iEcont][i] = packet_energies_unpacked[algo][i];
	  branchData_int_unpacker[iEcont][i]      = unpack5E4MToUnsigned(packet_energies_unpacked[algo][i]);
     // std::cout << "algo = " << algo << " starting energy = " << std::to_string(packet_energies_unpacked[algo][i]) + " unpacked from 5E4M to int = " + std::to_string(branchData_int_unpacker[iEcont][i]) << std::endl;
          branchData_raw_location[iEcont][i]  = packet_locations[algo][i];
          branchData_unpacker_loc[iEcont][i]  = packet_locations_unpacked[algo][i];
        }
	if (algo == "BC") { branchData_modSum[iEcont] = module_sum; }
	iEcont += 1;
      }
      event = nEvents;
    }
    tree->Fill();
    if(nEvents==n_Events) break; //for easier debuging
  }
  delete r;

  if (nEvents > 0) {
    // make directory for ROOT file
    std::string dir = "outputJul24_partial/Relay" + std::to_string(relayNumber) + "/BX_window_" + std::to_string(BXNumbers) + "/";
    std::string command = "mkdir -p " + dir;
    system(command.c_str());

    // open ROOT file
    std::string filename = dir + "Run" + std::to_string(runNumber) + "_train_" + std::to_string(trainNumber) + std::to_string(aligned) + std::to_string(wantedBX) + ".root";
    std::cout << filename << std::endl;
    TFile *file = TFile::Open(filename.c_str(),"RECREATE");

    // create ROOT directory for train
    std::string train_dir_name;
    train_dir_name = "train_" + std::to_string(trainNumber);
    if (aligned)  train_dir_name = train_dir_name + "/aligned/";
    else train_dir_name = train_dir_name + "/BX" + std::to_string(wantedBX) + "/";
    TDirectory *train = file->mkdir(train_dir_name.c_str());
    train->cd();

    file->cd(train_dir_name.c_str());
    tree->Write();
    delete file;
  }
  else {
    std::cout << "File had no events!" << std::endl;
    return 1;
  }

  return 0;
}
