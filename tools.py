def getRelayNumber(path):
  return path.split("Relay")[1].split("/")[0]

def getRunNumber(path):
  return path.split("Run")[1].split(".root")[0]

def getEcontNumber(path):
  return path.split("econt")[1].split("/")[0]

def getNEntries(f):
  return f[f"BX0/energyc_STC4_0"].values().sum()

def getPlotOutdir(path, mode = 1, extrastring = ""):
  runNumber = getRunNumber(path)
  relayNumber = getRelayNumber(path)
  econtNumber = getEcontNumber(path)
  if (mode == 1):
      outdir = "/".join(path.split("/")[:-1]).replace("output", "plots") + f"/Run{runNumber}"
  elif (mode == 2):
      outdir = "plots" + extrastring + "/Relay" + relayNumber + "_Run" + runNumber + "_econt" + econtNumber

  return outdir
