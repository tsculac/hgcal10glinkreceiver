import matplotlib.pyplot as plt
import matplotlib
import numpy as np

class Shape:
  def get_x(self):
    return self.points[:,0]

  def get_y(self):
    return self.points[:,1]

  def get_c(self, norm):
    cmap = matplotlib.cm.get_cmap('viridis')
    rgba = cmap(self.value / norm)
    return rgba

  def getCenter(self):
    x = np.mean(np.array(self.points)[:,0])
    y = np.mean(np.array(self.points)[:,1])
    return x, y

  def rotate180(self):
    self.points = -self.points

  def rotate30(self):
    """
    Rotate the points by 30 degrees counterclockwise.
    """
    self.points = np.array(self.points)
    angle = np.deg2rad(30)  # Convert 30 degrees to radians
    cos_theta = np.cos(angle)
    sin_theta = np.sin(angle)

    # Define the rotation matrix for 30 degrees
    rotation_matrix = np.array([
        [cos_theta, -sin_theta],
        [sin_theta, cos_theta]
    ])

    # Apply the rotation matrix to each point
    self.points = self.points @ rotation_matrix.T

  def translation(self, offset_x, offset_y):
    self.points[:,0] = self.points[:,0] + offset_x
    self.points[:,1] = self.points[:,1] + offset_y

class Hexagon(Shape):
  def __init__(self, x_center, y_center, base):
    self.x_center = x_center
    self.y_center = y_center
    self.base = base

    width = np.sqrt(3) * (base/2)
    height = np.sqrt(width**2 + base**2)
    self.points = [
      [x_center-width, y_center+base/2],
      [x_center, y_center+height],
      [x_center+width, y_center+base/2],
      [x_center+width, y_center-base/2],
      [x_center, y_center-height],
      [x_center-width, y_center-base/2]
    ]
    self.points = np.array(self.points)

class Diamond(Shape):
  def __init__(self, x, y, base):
    self.x = x
    self.y = y
    self.base = base

    height = np.sqrt(3)*(base/2)
    x_jump = base/2

    self.points = [
      [x, y],
      [x+base, y],
      [x+base+x_jump, y+height],
      [x+x_jump, y+height]
    ]
    self.points = np.array(self.points)

  def generateSet(self):
    base = self.base/2
    x_jump = base/2
    height = np.sqrt(3)*(base/2)

    set = [Diamond(self.x, self.y, base),
           Diamond(self.x+base, self.y, base),
           Diamond(self.x+x_jump, self.y+height, base),
           Diamond(self.x+base+x_jump, self.y+height, base)]
    return set

class DiamondR(Shape):
  def __init__(self, x, y, base):
    self.x = x
    self.y = y
    self.base = base

    height = np.sqrt(3)*base
    width = base

    self.points = [
      [x, y],
      [x+width/2, y+height/2],
      [x+width, y],
      [x+width/2, y-height/2]
    ]
    self.points = np.array(self.points)

  def generateSet(self):
    base = self.base/2
    height = np.sqrt(3)*base
    width = base

    set = [DiamondR(self.x, self.y, base),
           DiamondR(self.x+width/2, self.y+height/2, base),
           DiamondR(self.x+width, self.y, base),
           DiamondR(self.x+width/2, self.y-height/2, base)]
    return set

class DiamondF(Shape):
  def __init__(self, x, y, base):
    self.x = x
    self.y = y
    self.base = base

    height = np.sqrt(3)*(base/2)
    x_jump = base/2

    self.points = [
      [x, y],
      [x+base, y],
      [x+base-x_jump, y+height],
      [x-x_jump, y+height]
    ]
    self.points = np.array(self.points)

  def generateSet(self):
    base = self.base/2
    height = np.sqrt(3)*(base/2)
    x_jump = base/2

    set = [DiamondF(self.x, self.y, base),
           DiamondF(self.x+base, self.y, base),
           DiamondF(self.x-x_jump, self.y+height, base),
           DiamondF(self.x-x_jump+base, self.y+height, base)]
    return set

def getCoarseDiamonds(base=1, offset_x=0, offset_y=0, n=1):
  b = base*n
  xj = b/2
  h = np.sqrt(3) * b/2

  wr = b
  hr = np.sqrt(3)*b

  cmap = matplotlib.cm.get_cmap("Spectral")

  if n==1:
    diamonds = [Diamond(0, 0, b), Diamond(b, 0, b), Diamond(xj, h, b), Diamond(b+xj, h, b),
                DiamondR(2*b, 0, b), DiamondR(2.5*b, h, b), DiamondR(3*b, 0, b), DiamondR(2.5*b, -hr/2, b),
                DiamondF(xj, -h, b), DiamondF(xj+b, -h, b), DiamondF(xj*2, -h*2, b), DiamondF(xj*2+b, -h*2, b)]
  else:
    diamonds = [Diamond(0, 0, b/2), 
                DiamondR(b/2, 0, b/2), 
                DiamondF(xj/2, -h/2, b/2)]

  for dia in diamonds:
    dia.x += offset_x
    dia.y += offset_y
    dia.points[:,0] += offset_x
    dia.points[:,1] += offset_y
  return diamonds

def plotDiamonds(diamonds, norm, cmap):
  for dia in diamonds:
    plt.fill(dia.get_x(), dia.get_y(), color=cmap(norm(dia.value)) )

if __name__=="__main__":
  diamonds = getCoarseDiamonds()
  for i, dia in enumerate(diamonds):
    dia.value = i/12

  for dia in diamonds:
    plt.fill(dia.get_x(), dia.get_y(), color=dia.get_c())
  plt.colorbar(matplotlib.cm.ScalarMappable(cmap='viridis', norm=plt.Normalize(vmin=0, vmax=1)))
  #plt.show()

  granular_diamonds = []
  for dia in diamonds:
    granular_diamonds += dia.generateSet()
  for i, dia in enumerate(granular_diamonds):
    dia.value = i/48
  for dia in granular_diamonds:
    plt.fill(dia.get_x(), dia.get_y(), color=dia.get_c())
  plt.show()

def plotOutlines(shapes, fmt="k-", linewidth=1):
  for shape in shapes:
    x = shape.get_x()
    y = shape.get_y()
    xc = np.concatenate([x, x[:1]])
    yc = np.concatenate([y, y[:1]])
    plt.plot(xc, yc, fmt, linewidth=linewidth)
    plt.savefig("provaSTC16.png")
