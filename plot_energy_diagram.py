import argparse
import uproot
import hexplot
import sys
import matplotlib.pyplot as plt
import numpy as np
import os
import tools
import ROOT

import matplotlib.colors as mcolors
import matplotlib.cm as cm

def total_plot(STCs4, TCs4, locations_STC4, STCs16, TCs16, locations_STC16, TCs_BC, run, BX, train, tag):
    plt.figure(figsize=(12, 10))
    norm, cmap = create_color_norm_and_cmap(STCs4, STCs16, TCs_BC)
    # plt STC16
    hexplot.plotDiamonds(STCs16, norm, cmap)
    hexplot.plotOutlines(STCs16, linewidth=1)
    for i, STC16 in enumerate(STCs16):
      hexplot.plotOutlines(TCs16[i], linewidth=0.5)
      plt.text(STC16.getCenter()[0], STC16.getCenter()[1], STC16.label, color="r", horizontalalignment="center", verticalalignment="center")
      for i, TC in enumerate(TCs16[i]):
        plt.text(TC.points[0][0], TC.points[0][1], f"TC{i}", color="k", fontsize=8, horizontalalignment="left", verticalalignment="bottom")
    x, y = zip(*locations_STC16)
    plt.scatter(x, y, color="k", marker="x", zorder=10)


    # plt STC4
    hexplot.plotDiamonds(STCs4, norm, cmap)
    hexplot.plotOutlines(STCs4, linewidth=1)
    for i, STC4 in enumerate(STCs4):
      plt.text(STC4.getCenter()[0], STC4.getCenter()[1], STC4.label, color="r", horizontalalignment="center", verticalalignment="center")
    all_TCs = np.array(TCs4).flatten()
    hexplot.plotOutlines(all_TCs, linewidth=0.5)
    for i, TC_set in enumerate(TCs4):
        for i, TC in enumerate(TC_set):
          plt.text(TC.points[0][0], TC.points[0][1], f"TC{i}", color="k", fontsize=8, horizontalalignment="left", verticalalignment="bottom")
    x1, y1 = zip(*locations_STC4)
    plt.scatter(x1, y1, color="k", marker="x", zorder=10, label="Average position of highest energy deposit")

    # plt BC
    hexplot.plotDiamonds(TCs_BC, norm, cmap)
    hexplot.plotOutlines(TCs_BC, linewidth=1)
    for i, TC in enumerate(TCs_BC):
      plt.text(TC.points[0][0], TC.points[0][1], f"TC{i}", color="k", fontsize=8, horizontalalignment="left", verticalalignment="bottom")

    # plt graphics
    plt.xlabel('x position [cm]')
    plt.ylabel('y position [cm]')
    plt.axis('equal')   #cb = plt.colorbar(matplotlib.cm.ScalarMappable(cmap='viridis', norm=matplotlib.colors.Normalize(vmin=0, vmax=norm)))
    sm = plt.cm.ScalarMappable(cmap=cmap, norm=norm)
    plt.colorbar(sm, label="Energy")

    plt.title(f"Run {run} {tag} ")
    plt.savefig(f"Run{run}_BX{BX}_train_{train}_{tag}.png")
    plt.savefig(f"Run{run}_BX{BX}_train_{train}_{tag}.pdf")

def create_color_norm_and_cmap(STCs4, STCs16, TCs_BC):
    all_values = [STCs.value for STCs in STCs4] + [STCs.value for STCs in STCs16] + [STCs.value for STCs in TCs_BC]
    # Collect all values from both datasets
    print("ALL VALUES", all_values)
    # Define the normalization range
    norm = mcolors.Normalize(vmin=min(all_values), vmax=max(all_values))
    # Define the colormap
    cmap = cm.viridis
    return norm, cmap

def getMean(file_path, tree_name, leaf_name, BX, train):
    # Open the ROOT file
    with uproot.open(file_path) as file:
        # Access the TTree
        tree = file[f"train_{train}/BX1/"+tree_name]

        # Read data from the leaf
        leaf_data = tree[leaf_name].array(library="np")  # Load data as a numpy array

    # Flatten the array if it's multi-dimensional
    if leaf_data.ndim > 1:
        leaf_data = leaf_data.flatten()

    # Calculate the mean
    if leaf_data.size == 0:
        return 0
    else:
        return np.mean(leaf_data)

def getMeanLocation(file_path, tree_name, STC_number, TCs, BX, data_type, expected_values, econ_n, STCid, train):
  if STC_number >= 6:
        return 0, 0  # Return default coordinates
  else:
        with uproot.open(file_path) as file:
            tree = file[f"train_{train}/BX1/"+tree_name]
            branch_location = f"unpacker_loc{econ_n}_STC{STCid}_{STC_number}"
            leaf_data = tree[branch_location].array(library="np")  # Load data as a numpy array
            leaf_data = leaf_data.flatten()

            unique_values, counts = np.unique(leaf_data, return_counts=True)
            counts_dict = dict.fromkeys(expected_values, 0)
            counts_dict.update(dict(zip(unique_values, counts)))

            all_values = np.array(list(counts_dict.keys()))
            all_counts = np.array(list(counts_dict.values()))

            # Normalize counts
            counts_sum = all_counts.sum()
            normalized_counts = all_counts / counts_sum if counts_sum > 0 else all_counts

            # Compute weighted averages using TCs
            x = sum([normalized_counts[i] * TCs[i].getCenter()[0] for i in range(len(TCs))])
            y = sum([normalized_counts[i] * TCs[i].getCenter()[1] for i in range(len(TCs))])
            return x, y

def compute_STC16(args):
    print("Processing STC16..")
    branch_energy = [f"energy_int_unpacker2_STC16_{STC}" for STC in range(3)]
    tree_name = "tree"

    all_means = [getMean(args.inFile,tree_name,branch_name,args.BX, args.train)/16 for branch_name in branch_energy] #for now only considering BX=0
    print("all means", all_means)
    #ped_means = np.array([getMean(f, hist_name, pedastal_BX) for hist_name in hist_names])
    means = all_means #- ped_means #for now we are plotting only the mean energy (no pedestals)
    #all_means_maxes = [max(means) for means in all_means]
    #nominal_BX = np.argmax(all_means_maxes)
    #means = all_means[nominal_BX]

    base = 166.8/(2*np.sqrt(3))
    STCs16 = hexplot.getCoarseDiamonds(base=base*2, offset_x=-2*base, n=2)

    #nominal_BX = "0"
    remapping = [0,1,2]
    assert len(np.unique(remapping)) == 3
    for i, STC in enumerate(STCs16):
        STC.value = means[remapping[i]]
        STC.label = f"STC{remapping[i]}"

    expected_values = np.array([0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15])
    TCs_per_STCs = []
    for i, STC16 in enumerate(STCs16):
        STCs4 = STC16.generateSet()
        STCs4 = np.array(STCs4).flatten()
        all_TCs=[]
        for TCs in STCs4:
            TCs= TCs.generateSet()
            all_TCs.extend(np.array(TCs).flatten())
        TCs_per_STCs.append(all_TCs)

    if args.train == 0 or args.train == 1:
      for STC in STCs16:
        STC.rotate30()
        STC.translation(0, -2*base)
      for STC in TCs_per_STCs:
        for TC in STC:
          TC.rotate30()
          TC.translation(0, -2*base)

    locations_STC16 = []
    for i, STC16 in enumerate(STCs16):
        econ_n ="2"
        STCid = "16"
        x, y = getMeanLocation(args.inFile, tree_name, i, TCs_per_STCs[i], args.BX, args.type, expected_values, econ_n, STCid, args.train)
        locations_STC16.append((x,y))

    return STCs16, TCs_per_STCs, locations_STC16


def compute_STC4(args):
    print("Processing STC4..")
    branch_energy = [f"energy_int_unpacker1_STC4_{STC}" for STC in range(6)]
    tree_name = "tree"

    all_means = [getMean(args.inFile,tree_name,branch_name,args.BX,args.train)/4 for branch_name in branch_energy] #for now only considering BX=0
    #ped_means = np.array([getMean(f, hist_name, pedastal_BX) for hist_name in hist_names])
    means = all_means #- ped_means #for now we are plotting only the mean energy (no pedestals)
    print("all means", all_means)
    #all_means_maxes = [max(means) for means in all_means]
    #nominal_BX = np.argmax(all_means_maxes)
    #means = all_means[nominal_BX]

    base = 166.8/(2*np.sqrt(3))
    STCs = hexplot.getCoarseDiamonds(base=base, offset_x=-2*base)

    #remapping = list(range(12))
    remapping = [0,1,2,4,3,5,6,7,8,9,10,11]
    assert len(np.unique(remapping)) == 12
    for i, STC in enumerate(STCs):
      means.extend([0] * 6)
      STC.value = means[remapping[i]]
      STC.label = f"STC{remapping[i]}"

    TCs = [STC.generateSet() for STC in STCs]

    # Remap TCs to match the new STC order
    # Create a new list of TC sets based on the remapping
    remapped_TCs = [None] * len(TCs)
    for i, remapped_index in enumerate(remapping):
        remapped_TCs[remapped_index] = TCs[i]

    if args.train == 0 or args.train == 1:
      for STC in STCs:
        STC.rotate30()
        STC.translation(-np.sqrt(3)*base, base)
      for TC_set in remapped_TCs:
        for TC in TC_set:
          TC.rotate30()
          TC.translation(-np.sqrt(3)*base, base)

    locations_STC4 = []
    for i, TC_set in enumerate(remapped_TCs):
        expected_values = np.array([0, 1, 2, 3])
        econ_n ="1"
        STCid = "4"
        x, y = getMeanLocation(args.inFile, tree_name, i, TC_set, args.BX, args.type, expected_values, econ_n, STCid, args.train)
        locations_STC4.append((x,y))

    return STCs, remapped_TCs, locations_STC4

def compute_BC(args):
    print("Processing BC..")
    base = 166.8/(2*np.sqrt(3))
    #nominal_BX ="0"

    with uproot.open(args.inFile) as file:
        tree = file[f"train_{args.train}/BX1/tree"]

        # Define the branch prefixes
        location_prefix = 'unpacker_loc0_BC_'
        energy_prefix = 'energy_int_unpacker0_BC_'
        num_branches = 6  # Assuming you have branches from 0 to 5

        # Dictionary to store combined data
        combined_location_data, combined_energy_data = [], []

        # Iterate over each branch
        for i in range(num_branches):
            location_branch = f'{location_prefix}{i}'
            energy_branch = f'{energy_prefix}{i}'

            # Check if branches exist
            if location_branch not in tree.keys():
                print(f"Warning: Branch {location_branch} not found.")
                continue

            # Read the branches into numpy arrays
            location_data = tree[location_branch].array()
            energy_data = tree[energy_branch].array()

            # Filter data based on event ID if specified
            if args.spec_event is not None:
                mask = tree['EventNumber'].array() == args.spec_event
                location_data = location_data[mask]
                energy_data = energy_data[mask]

            combined_location_data.extend(location_data)
            combined_energy_data.extend(energy_data)

        # Convert lists to numpy arrays
        combined_location_data = np.array(combined_location_data)
        combined_energy_data = np.array(combined_energy_data)

        # Process data as needed
        location_energy_dict = {}
        for locations, energies in zip(combined_location_data, combined_energy_data):
            if locations not in location_energy_dict:
                location_energy_dict[locations] = []
            location_energy_dict[locations].append(energies)

        # Convert lists to numpy arrays for better performance
        for loc in location_energy_dict:
            location_energy_dict[loc] = np.array(location_energy_dict[loc])

        # Calculate mean energy for each location
        location_mean_energy_dict = {loc: np.mean(energies) for loc, energies in location_energy_dict.items()}

        # Ensure all 48 locations are present in the dictionary, initializing missing ones with zero energy
        all_locations = set(range(48))
        for loc in all_locations:
            if loc not in location_mean_energy_dict:
                location_mean_energy_dict[loc] = 0.0

        STCs = hexplot.getCoarseDiamonds(base=base, offset_x=-2*base)
        TCs = []
        for dia in STCs:
            TCs += dia.generateSet()

        # Remapping list
        # remapping = list(range(48))
        remapping = [0,1,12,3,4,5,6,7,13,9,10,11,2,8,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47]

        # Update TCs with mean energies
        for i, TC in enumerate(TCs):
            if i < len(remapping):  # Ensure index is within range
                location = remapping[i]
                TC.value = location_mean_energy_dict.get(location, 0)  # Get mean energy or default to 0
                TC.label = f"TC{location}"

        if args.train == 0 or args.train == 1:
          for STC in STCs:
            STC.rotate30()
            STC.translation(np.sqrt(3)*base, base)
          for TC in TCs:
            TC.rotate30()
            TC.translation(np.sqrt(3)*base, base)

    return TCs

def find_event(args):
    with uproot.open(args.inFile) as file:
      tree = file[f"train_{args.train}/BX1/tree"]
      energy_BC = tree['energy_5E4M_unpacker0_BC_1'].array()
      numpy_array = np.asarray(energy_BC).flatten()
      indices_BC = np.where(numpy_array>=128)[0]
      # energy_STC4 = tree['energy_5E4M_unpacker1_STC4_3'].array()
      # numpy_array = np.asarray(energy_STC4).flatten()
      # indices_STC4 = np.where(numpy_array>=160)
      # common_indices = np.intersect1d(indices_BC, indices_STC4)
      print(indices_BC)
      args.spec_event = indices_BC[1]

def parse_arguments():
    parser = argparse.ArgumentParser(description="Process and plot energy data.")
    parser.add_argument("--inFile", type=str, help="Path to the ROOT file.")
    parser.add_argument("--run",    type=str, help="Run number")
    parser.add_argument("--tag",    type=str, help="tag")
    parser.add_argument("--BX",  type=str, help="BX number", default="7")
    parser.add_argument("--type",   type=str, choices=['BC', 'STC4', 'STC16'], help="Type of data to process.")
    parser.add_argument("--spec_event", type=int, help="Specify the event you want to process.", default=None)
    parser.add_argument('--train', type=int, help='Choose which train to plot')
    return parser.parse_args()

if __name__ == "__main__":
    ''' python3 plot_energy_diagram.py --inFile /eos/user/m/mchiusi/BeamTestJul24/Run1722382405_tree.root --run 1722382405 --tag test --train 0 '''
    args = parse_arguments()

    # find_event(args)
    if args.train == 0 or args.train == 1:
      TCs_BC = compute_BC(args)
      STCs4, TCs4, locations_STC4 = compute_STC4(args)
      STCs16, TCs16, locations_STC16 = compute_STC16(args)
      total_plot(STCs4, TCs4, locations_STC4, STCs16, TCs16, locations_STC16,  TCs_BC, args.run, args.BX, args.train, args.tag)

    elif args.type == "BC": compute_BC(args)
    elif args.type == "STC4": compute_STC4(args)
