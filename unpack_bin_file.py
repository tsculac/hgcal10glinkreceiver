import subprocess
import argparse

def run_shell_command(command):
    try:
        subprocess.run(command, check=True, shell=True)
    except subprocess.CalledProcessError as e:
        print(f"Command '{command}' failed with error: {e}")

if __name__ == "__main__":
    ''' python3 unpack_bin_file.py --relay 1722267448 --run 1722267448 --tag test --train 0 1 --nEvents 1000 '''
    parser = argparse.ArgumentParser(description="Run econt_processor and hadd commands.")
    parser.add_argument("--relay",     type=int, required=True,  help="Relay number")
    parser.add_argument("--run",     type=int, required=True,  help="Run number")
    parser.add_argument("--tag",     type=str, required=False, help="Tag for the output root file")
    parser.add_argument("--train",   type=int, nargs=2, required=True, help="Range of trains (start and end inclusive)")
    parser.add_argument("--nEvents", type=int, required=True,  help="Choose the number of events to process")

    args = parser.parse_args()

    # compile.sh
    run_shell_command("sh compile.sh")

    # ./econt_processor.exe command for each BXNumber
    trains = list(range(args.train[0], args.train[1] + 1))
    for train in trains:
        command = f"./econt_processor.exe {args.relay} {args.run} {train} 7 {args.nEvents} 1 3"
        run_shell_command(command)
        for BXnumber in range(7):
          command = f"./econt_processor.exe {args.relay} {args.run} {train} 7 {args.nEvents} 0 {BXnumber}"
          run_shell_command(command)

    # hadd command
    output_file = f"outputJul24_partial/Relay{args.relay}/Run{args.run}_{args.tag}.root"
    input_files = " ".join([f"outputJul24_partial/Relay{args.relay}/BX_window_7/Run{args.run}_train_{train}0{BXnumber}.root" for train in trains for BXnumber in range(7)])
    aligned = " ".join([f"outputJul24_partial/Relay{args.relay}/BX_window_7/Run{args.run}_train_{train}13.root" for train in trains])
    hadd_command = f"hadd -f {output_file} {input_files} {aligned}"
    run_shell_command(hadd_command)
